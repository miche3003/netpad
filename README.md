# Netpad

##Just another web based code editor.

This is an instructional project aimed to build a minimal 
but full-featured code editor in the browser!

**Give it a [try](http://covodiattila.altervista.org/netpad/ "Demo") and [tell me](http://twitter.org/#!/miche3003) what you think about or [notify](http://bitbucket.org/miche3003/netpad/issues/new) a bug**

## Demos

There are currently 2 demos:

- [Single Netpad editor](http://covodiattila.altervista.org/netpad/index.html)
- [Three editors in a page](http://covodiattila.altervista.org/netpad/triple.html)

##Features

    - Syntax highlighting (javascript only supported, for now)
    - Multiple editors per page
    - ...others yet to come!

## Author
Michele Carignani (carignani at cli.di.unipi.it)

## License
This project has not been released so far.

## Inspirations
- Surely the almost-perfect web editor [Ace](ace.ajax.org) by [Ajaxorg](ajax.org), built from Bespin and SkyWriter
    
