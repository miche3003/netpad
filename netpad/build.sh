#!/bin/bash

##
##
##
##
##

BUILDER="node ./node_modules/requirejs/bin/r.js"

VERSION=0.8
REVISION=$(git log -n 1 --pretty=format:%h)

TIMESTAMP=$(git log -n 1 --pretty=format:%at)
DATE=$(date -d "1970-01-01 $TIMESTAMP sec" "+%c")

VER_PREG=s/%VERSION/$VERSION/g
REV_PREG=s/%REVISION/$REVISION/g
DAT_PREG=s/%DATE/$DATE/g

HTML=$(ls ./html/)

USAGE="Usage: $0 { all | app | demos | css | libs }"

if [ $# -lt 1 ] ; then
	echo $USAGE
	exit 0
fi

## Build the javascript files (minified and development versions)
function app {
	
	$BUILDER -o name=main out=build/js/Netpad-$VERSION-dev.js baseUrl=./src optimize=none
	$BUILDER -o name=main out=build/js/Netpad-$VERSION-min.js baseUrl=./src optimize=uglify	
}

## Copy require.js and mootools
function libs {
	cp lib/require.js build/js/require.js
	cp lib/mootools.js build/js/mootools.js
}

## Build the unified minified css
function css {
	$BUILDER -o cssIn=css/netpad.css out=build/css/Netpad.css
}

## Copy and update the demos
function demos {
	for file in $HTML
	do
		sed "$VER_PREG" html/$file | sed "$REV_PREG" | sed "$DAT_PREG" > build/$file
	done
}

function all {
	app
	libs
	demos
	css
}

case $1 in
	all|app|demos|css|libs)
		$1;;
	*)
		echo $USAGE;;
esac

exit 0

#build-require :
#	$(BUILDER) -o baseUrl=./src paths.requireLib=../../require name=main \
#	include=requireLib out=build/main-built-rjs.js	
	

