/** 
 * 
 *  @author Michele Carignani
 */
/*jslint indent: 4, newcap: true, plusplus: true */
/*global define: true */

// "use strict";

define(function (require, exports, module) {

    Tokenizer = function (lang) {
        //@todo controllo argomento
        
        var Ld = new require('langs/' + lang).LanguageDescriptor;
        this.l = new Ld();

        this.setLanguage = function(new_lang) {
            try{
                var New_Ld = new require('langs/' + lang).LanguageDescriptor;
            } catch (e) {
                alert('No language '+ new_lang);
                return;
            }
            this.l = new New_Ld();
        }
        
        this.setLang = function (lang) {
            var ld = require(['langs/' + lang], (function (scope) {
                return function(lang){
                    console.log(scope);
                    scope.l = new lang.LanguageDescriptor();
                };
            })(this));
        }

        var equalFn = function (x) {
            return function (y) {
                return x === y;
            }
        }
        
        this.token = function (t) {
            var res = {txt : t, type : 'unknown', name : 'unknown'}, o;
            if(t == " "){
                res.type = 'whitespace';
                res.name = 'space';
                res.str = 'np_token np_whitespace';
            }
            if(t == "\t"){
                res.type = 'whitespace';
                res.name = 'tab';
                res.str = 'np_token np_whitespace np_tab';
            }
            if(t.substr(0,2) == "//"){
                res.type = 'comment';
                res.name = 'inline comment';
                res.str = 'np_token np_comment';
            }
            // is a reserved word of the language?
            if (this.l.reserved.hasOwnProperty(t)) {
                res.type = 'reserved';
                res.name = this.l.reserved[t];
                res.str = 'np_token np_reserved np_' + res.name;
            }
            // is a literal?
            if (this.l.literals.hasOwnProperty(t)) {
                res.type = 'literal';
                res.name = this.l.literals[t];
                res.str = 'np_token np_literal np_' + res.name;
            }
            // is a string?
            if ( t[0] === t[t.length - 1] ) {
                if (this.l.str_marks.some(equalFn(t[0]))){
                    res.type = 'string';
                    res.str = 'np_token np_STRING';
                }                
            }
            o = this.l.isDigits(t);
            if (o.res) {
                res = o.val;
                res.txt = t;
                res.str = "np_token np_literal np_" + res.name;
            }
            return res;
        };
        
        this.isTokenEnding = function (c) {
            return this.l.tokenEnding.indexOf(c) !== -1;
        };

        this.isStringMark = function (c) {
            return this.l.str_marks.indexOf(c) !== -1;
        };
        
        this.analize = function(txt) {
            var lines = txt.split('\n');
            var res = [], INLINE_COMMENT = false, o, c, toks;
            for(var i = 0; i < lines.length; i++){
                // console.log(lines[i]);
                toks = this.parse(lines[i]);
                res[i] = [];
                INLINE_COMMENT = false;
                for(var j = 0; j < toks.length; j++ ){
                        o = this.token(toks[j]);
                        c = { txt : toks[j], type : 'comment', name : 'inline comment', str : 'np_token np_comment' };
                        INLINE_COMMENT = ( toks[j] === '//' || INLINE_COMMENT);
                        if(toks[j].substr(0,2) == '//') console.log(toks[i]);
                        res[i][j] = (INLINE_COMMENT && o.type !== 'whitespace') ? c : o;
                }
            }
            return res;
        }

        /**
            returs only the class of all tokens
        */
        this.markup = function(txt) {
            var lines = txt.split('\n');
            var res = [], INLINE_COMMENT = false, o, c, toks;
            for(var i = 0; i < lines.length; i++){
                toks = this.parse(lines[i]);
                res[i] = [];
                INLINE_COMMENT = false;
                for(var j = 0; j < toks.length; j++ ){
                        o = this.token(toks[j]);
                        c = 'np_token np_comment';
                        INLINE_COMMENT = ( toks[j] === '//' || INLINE_COMMENT);
                        res[i][j] = (INLINE_COMMENT && o.type !== 'whitespace') ? c : o.str;
                }
            }
            return res;
        }

        this.parse = function (str) {
            var res = [],
                token = 0,
                i = 0,
                BASE = 0,
                IN_STRING_LITERAL = 1,
                IN_STRING_LITERAL_ESCAPED = 2,
                state = BASE,
                stringOpener = '';

//            if(str.substr(0,2) == "//"){
//                return [ str ];
//            }

            while (i < str.length) {
                
                if (this.isTokenEnding(str[i])) {
                    switch (state) {
                        case IN_STRING_LITERAL_ESCAPED:
                            state = IN_STRING_LITERAL;
                            break;
                        case BASE:
                            token = (res[token] === undefined) ? token : token + 1;
                            res[token] = str[i];
                            token++;
                            break;
                        default:
                            break;
                    }
                } else if (this.isStringMark(str[i])) {
                    switch(state){
                        case IN_STRING_LITERAL:
                            if(str[i] === stringOpener){
                                res[token] = res[token] + str[i];
                                token++;
                                state = BASE;
                            } else {
                                res[token] = (res[token] === undefined) ? str[i] : res[token] + str[i];
                            }
                            break
                        case BASE:
                            token = (res[token] === undefined) ? token : token + 1;
                            res[token] = str[i];
                            state = IN_STRING_LITERAL;
                            stringOpener = str[i];
                            break
                        case IN_STRING_LITERAL_ESCAPED:
                            state = IN_STRING_LITERAL;
                            break
                    }
                } else {
                   res[token] = (res[token] === undefined) ? str[i] : res[token] + str[i];
                   if( str[i] === '\\'){
                        if( state === IN_STRING_LITERAL)
                            state = IN_STRING_LITERAL_ESCAPED;
                   }
                }
                
                i++;
            }
            return res;
        };
    };
    
    exports.Tokenizer = Tokenizer;
});

