
// @todo documentare

/* ***************************************
 *      Javascript Plugin
 * ************************************** */     

define(function(require, exports, module){
 
 var Javascript = function(){
    /**
     *  @description list of reserved word of the javascript language
     */
    this.reserved = { 
        'function' : "FUNCTION",
        'return'   : "RETURN",
        'var'      : "VAR_DECL",
        'if'       : "IF_STAT",
        'switch'   : "SWITCH_STAT",
        'else'     : "ELSE_STAT",
        'for'      : "FOR_STAT",
        'while'    : "WHILE_COM",
        'break'    : "BREAK_COM",
        'throw'    : "THROW_COM",
        'new'      : "NEW_COM"
    };

    this.literals = {
        'true' : 'BOOL_TRUE',
        'false' : 'BOOL_FALSE',
        'null' :  'NULL',
        'undefined' : 'UNDEF',
        'this' : 'THIS'
    };
    
    // Le cifre sono permesse nelle variabili, perciò un numero deve
    // essere composto solo da cifre
    this.isDigits = function( /* String */ t ){
        if( t === undefined ) { throw new Error("undefined string to parse"); }
        for(var i = 0; i < t.length; i++)
            if( isNaN(parseInt(t[i])) )
                return {res : false };
        return { res : true, val : { type: "literal", name : "number" } };
    }


//     KeyCodes         - TODO Sono standard?!?!
//     \n           13 
//     backspace    8
//
    this.operators = [ '+', '-', '*', '+=', '*=', '<', '>', '==', '>=', '<=', '===' ]
    this.tokenEnding = [ ' ', '\n', '\t',  ':', '(', ')', '{', '}', ".", ";", "=" ];

    for (var i = this.operators.length - 1; i >= 0; i--) {
        this.tokenEnding.push(this.operators[i]);
    };

    this.str_marks = [ '"', "'" ];
    this.blockEnding = [')', '}'];

 }
 
 exports.LanguageDescriptor = Javascript;
 
});