
// @todo documentare

/* ***************************************
 *      Javascript Plugin
 * ************************************** */     

define(function(require, exports, module){
 
 var Html = function(){
    /**
     *  @description list of reserved word of the javascript language
     */
    this.reserved = { 
        'a'        : "ANCHOR"
        , 'div'      : "RETURN"
        , 'b'      : "VAR_DECL"
        , 'html'       : "IF_STAT"
        , 'body'   : "SWITCH_STAT"
        , 'head'     : "ELSE_STAT"
        ,'script'      : "FOR_STAT"
        ,'style'      : "FOR_STAT"
    };

    this.literals = {
        'true' : 'BOOL_TRUE',
        'false' : 'BOOL_FALSE',
        'null' :  'NULL',
        'undefined' : 'UNDEF',
        'this' : 'THIS'
    };
    
    // Le cifre sono permesse nelle variabili, perciò un numero deve
    // essere composto solo da cifre
    this.isDigits = function( /* String */ t ){
        if( t === undefined ) { throw new Error("undefined string to parse"); }
        for(var i = 0; i < t.length; i++)
            if( isNaN(parseInt(t[i])) )
                return {res : false };
        return { res : true, val : { type: "literal", name : "number" } };
    }


//     KeyCodes         - TODO Sono standard?!?!
//     \n           13 
//     backspace    8
//

    this.tokenEnding = [ ' ', '\n', '\t', '<', '>'];
    this.str_marks = [ '"', "'" ];
    this.blockEnding = [')', '}'];

 }
 
 exports.LanguageDescriptor = Html;
 
});