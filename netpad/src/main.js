 require.config({
    baseUrl: "netpad/src",
    paths: {
        "lib": "../lib",
        "css" : "../css"
    },
    waitSeconds: 5,
    locale: "it-it"
});

require([ "Netpad", "Buffer", "Display", "Editor", "Cursor", "input/keyboardInput", 
            "input/mouseInput","input/Clipboard", "Tokenizer", "langs/javascript", 'Selection',
           // "parse-js", "crock_parse", 
           "tokens", "Listbox", "math"],  
       function () {
        // Go!
        require.ready(function(){
            var np = new (require("Netpad").Netpad)();
            var MathLib = require('math').MathLib;
            netpad.editors[0].listbox.addItems(MathLib);
       })
});
