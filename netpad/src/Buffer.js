

/**
 *  @fileoverview Definisce la struttura che contiene e modifica la rappresentazione interna del testo di un documento
 *  
 *  @author Michele Carignani
 *  
 */
define(function(require, exports, module){

    /** @class */
    Cell = function ( c , at ){
           
           this.attr = {}
           this.parent = null;
           this.$c = c;
           if (at != undefined )
               for (var key in at)
                   if ( ! this.hasOwnProperty( at ) )
                        this.attributes[key] = at[key];
           
           this.content = function(){return this.$c;}

           this.length = function(){return 1;}
    }
     
    /**
     * @class Contiene il testo dentro un array
     */
    ArrayStorage = function(){
         
         this.cells = new Array();
         this.point = 0; /** Point is an integer - the point is logically after that cell */
         
         /** restituisce la prima cella del buffer */
         this.start = function (){
            return this.cells[0];
         }
         
         this.end = function (){
            return this.cells[this.cells.length - 1];
         }

         this.atLocation = function ( loc ){
            return this.cells[loc];
         }
         
         /** Applies fnc to all members of the buffer 
          **/
         this.all = function ( fnc ){
             this.cells.forEach( fnc );
         }
         
         this.insertAtPointer = function( c ){
            this.cells.splice(this.point, 0, new Cell(c));
            this.point++;
         }
         
         this.insertAt = function( c, loc){
             this.cells.splice( loc , 0, new Cell(c));
         }
         
         /** Ritorna il numero di linee nel buffer */
         this.getLineNum = function(){
             var ret = 1;
             for( var i = 0; i < this.cells.length; i++)
                 if (this.cells[i].$c == '\n')
                     ret++;
             return ret;
         }
         
         this.setPointRelative = function( s ){
             this.point = this.point + s;
         }
         
         /*
          * Sets the point at the c-th charachter of the r-th line
          */
         this.setPoint = function (c, r){
             var i = 1, j = 0;
             /* Conta il numero di caratteri delle prime r-1 righe */
             while(i < r && j < this.cells.length ){
                 if( this.atLocation(j).$c == '\n') i++;
                 j++;
             }
             this.point = j + c;
         }
         
         /** Delete howmany characters starting from the point */
         this.deleteChars = function (howmany) {
             if ( howmany > 0){
                 this.cells.splice(this.point, howmany);
             } else {
                howmany *= -1;
                var diff = this.point - howmany;
                var start = ( diff > 0 ) ? this.point - howmany : 0,
                    off = (diff > 0) ? howmany : howmany - diff;
                this.cells.splice(start, off );
             }
         }
     }
 
    /** 
     *  @class Internal representation of the text of the document
     */
     Buffer = function (){
        
        this.charsNum = 0; /** Numero di caratteri nel  buffer */
        this.$content = new ArrayStorage();
     
        /** Empties the buffer */
        this.clear = function () {
                // @todo inutile avere due copie dello stesso codice
             this.$content = new ArrayStorage;
             this.charsNum = this.num_lines = 0;
             this.point = this.cur_line = this.start();
         };
         
         /** Deallocates all the memory ? */
         this.destroy = function () {
             this.$content = null;
             // @todo altro da cancellare ?
             // @todo se multi-buffer, lasciare la lista coerente
         }; 
         
         /** Return a pointer to the first Cell in buffer */
         this.start = function () { 
             return this.$content.start();
         };
         
          /** Ritorna il numero di caratteri nel buffer */
         this.getLineNum = function () {
             return this.$content.getLineNum();
         };
         
         /** Return a pointer to the last Cell in buffer */
         var end = function () { 
            return this.$content.end();
         };   
         
         this.pointSet = function ( c, r ) {
             this.$content.setPoint(c, r);
         };
     
         this.pointShift = function ( s ) { 
             this.$content.setPointRelative( s );
         };
         
         this.pointToBeginning = function(){
             this.pointSet(0, 0);
         }
         
         /* Absolute position in number of characters from the beginning
          * 
          * */
         this.pointGet = function () { 
             return this.$content.point;
         };
         
         this.pointGetLine = function () { 
             var str = this.dumpString();
             var c = 0, r = 1, p = this.pointGet();
             while (c < p){
                 if(str[c] == '\n')
                    r++;
                 c++;
             }
             return r;
         };
         
         this.getLines = function (){
             return this.dumpString().split('\n');
         }

        // returns the character after the point. 
        // Returns '' if the point is at the end of the buffer or in other special cases
        this.Get_Char = function(){
            var cell = this.Point.next();
            return (cell != null ) ? cell.$content : '' ;
        }

        // returns up to count characters starting from the point. 
        // It will return fewer than count characters if the end of the buffer is encountered.
        this.Get_String = function ( len ){
            return this.$content.getString( len );
        }
        
        // returns the number of characters in the buffer (i.e., the length of the buffer).
        this.getCharsNum = function() {            
            return this.$content.cells.length();
        }

        
        // returns the number of lines in the buffer. 
        // It is undefined whether or not one counts an incomplete last line.
        this.Get_Num_Lines  = function (){
            return this.getLines().length;
        }
         
     /** 
      inserts one character at the point. The point is placed after 
      the inserted character. 
      @param c a str of 1 character
     */
     this.insertChar =  function ( c ) {
         this.$content.insertAtPointer( c );
         this.charsNum += 1;
     }
    
    /** Inserts a string of characters at the point. The point is placed after the string. */
    this.insertString = function( str ){
        for(var i = 0; i < str.length; i++ ){
            this.insertChar( str[i] );
        }
    }
    
    // replaces one character with another. This routine is logically equivalent to:
    // insertChar(c); Delete(1);
    // but is potentially more efficient. If the point is at the end of the buffer, the routine simply does an insert.
    this.replaceChar = function ( content, loc ){
        var c;
        if ( loc != undefined )            
            c = this.getCell( loc );
        else
            c = this.getCell( this.$content.point - 1 );
        c.replace ( content );
    }
	 
    
    // Replace_String replaces a string as if Replace_Char is called on each of its characters.
    
    
    // removes the specified number of characters from the buffer. The specified number of characters are 
    // removed after the point if count is positive or before the point if count negative. 
    // If the specified count extends beyond the start or end of the buffer, the excess is ignored.
    this.deleteChars = function ( num ){
        this.$content.deleteChars( num );
    }
   
    /** Returns a string of content of the buffer */
        this.dumpString = function ( ){
            var t = "";
            this.$content.all( function( c ){
                t += c.content();
            });
            return t;
        }
    
    }
 
    exports.Buffer = Buffer;
    // exports.Cell = Cell;
    // exports.Location = Location;
});