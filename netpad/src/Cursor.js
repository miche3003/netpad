/**
 *  @fileoverview Abstraction for the Display Cursor element
 *  @author Michele Carignani
 *
 */
define(function (require, exports, module) {
    
    /** @class
     *  @description Abstraction for the Display Cursor element
     */
    Cursor = function (frame) {
        
        /** The div containing the cursor */
        this.frame = frame;
        /** The div representing the cursor*/
        this.el = new Element('div', {'class' : 'np_cursor'});
        this.el.setStyle('height', this.CHARS_HEIGHT);
        this.frame.adopt(this.el);
        
        /** x and y position of the cursor in number of characters (both 0 indexed)*/
        this.position = {x: 0, y: 0};

        this.visible = false;

        this.toOrigin = function (){
            this.set(0, 0);
        }
        
        this.show = function () {
            this.el.setStyle('display', 'block');
            this.visible = true;
        }

        this.hide = function (){
            this.el.setStyle('display', 'none');   
            this.visible = false;
        }

        this.blink = function () {
            if ( this.visible ){
                this.hide();
            }
            else
                this.show();
        }
    
        this.isOnFirstLine = function() {
            return this.position.y == 0;
        }

        this.basePosition = function(){
            return { 
                x: (this.position.x * this.CHARS_WIDTH) + 5, /* +13 possibly @bug */
                y: (this.position.y + 1) * this.CHARS_HEIGHT};
        }
    
        this.set = function (a, b, maxlength) {
            if (maxlength != undefined )
                this.position.x = (a > maxlength ) ? maxlength : a ;
            else
                this.position.x = a ;
            this.position.y = b;

            this.el.setStyles({
                'margin-left' : ((this.position.x * this.CHARS_WIDTH) + 5) + 'px' ,
                'margin-top' : (this.position.y * this.CHARS_HEIGHT) + 'px'
            });
        }
        
        
    }
    
    exports.Cursor = Cursor;
    
});

