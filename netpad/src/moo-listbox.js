
define(function(require, exports, module){
    
    var ListBox = function ( parent, items, callback ){
        
        this.box = new Element('div', { id : 'np_listbox'});
        
        this.box.parent = this;
        this.selected = 0;
        this.active = false;
        
        var ListItem = function( content, id ){
            
            var it =  new Element( 'a', { 
                href : "", 
                'class' : 'np_listbox_item',
                'text' : content,
                'id' : id
            });
            
            it.addEvents({
                    'click': function(e){
                        e.preventDefault();
                        console.log(this.get('text'));
                    },
                    'onfocus' : function( e ){ 
                        console.log("focus");
                        e.target.set("id", "selected"); 
                    },
                    'onblur' : function( e ){ 
                        e.target.set("id", ""); 
                    },
                    'keydown' : function (e) {
                        console.log("keydown");
                        switch (e.code) {
                            case 27: // Escape
                            case 8:  // BackSpace
                                e.data.lb.hide();
                                e.preventDefault();
                                e.stopPropagation();
                                break;
                            case 13: // Enter
                                callback( e.data.self.text() ); // Will retrieve and send data
                                e.data.lb.hide();
                                break;
                            case 40: // Down arrow
                                e.data.lb.down();
                                break;
                            case 38: // Up arrow
                                e.data.lb.up();
                                break;
                            default :break;
                        }
                        e.stopPropagation();
                    }
                });
            
            return it;
            
        }
        
        this.setItems = function (items) {
            // TODO prototipizzare gli event handlers !!
            var new_item;
            var listbox = this.box;
            for (var i = items.length - 1; i >= 0; i--) {
                var new_item = new ListItem( items[i], this.selected == i ? 'selected' : '' );
                listbox.adopt( new_item );
            }
        };
        
        this.hide = function() {
            this.box.setStyle('display', 'none');
            this.active = false;
        }
        
        this.show = function (pos) {
            if (! this.active ){
                
                if(pos)
                    this.setPosition(pos['left'], pos['top']);
                
                this.active = true;
                this.box.setStyle('display', 'block');
                console.log( $$('a[class="np_listbox_item"]:first-of-type') );
                $$('a[class="np_listbox_item"]:first-of-type').fireEvent(focus);
            }
        }
        
        this.getPosition = function (){
            return { y : this.box.getSyle('top') , x : this.box.getSyle('left')} ;
        }
        
        this.setPosition = function( x, y){
            this.box.setStyle( 'top', y);
            this.box.setStyle( 'left', x);
        }
        
        this.up = function (){ this.select( this.selected - 1); }
        
        this.down = function(){ this.select( this.selected + 1 ); }
        
        this.select = function (item_idx) {
            var children = $(this.box.children());
            this.selected = (item_idx < 0) ? 
                            children.length + ( item_idx % children.length ) : 
                            item_idx % children.length ;
            $(children[this.selected]).focus();
        }
        
        this.box.inject( parent );
        if ( items )
            this.setItems( items );
        
        this.box.addEvent('click', function (e) {
            window.lb.hide();
            e.stopPropagation();
        });
       
    };
   
   exports.ListBox = ListBox;
});