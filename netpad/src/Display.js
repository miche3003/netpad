
/** @fileoverview Visualizzazione nella pagina
*   @author Michele Carignani
*  
*
*/
/*jslint newcap : true, sloppy : false */
/*global define : true, $ : false, Element : true */

define(function (require, exports, module) {

    /** @class Display
     *  @description Takes care of manipulating the DOM 
     *  @arg element the DOM element in which must be drawn the editor
     **/
    Display = function (element) {

        var Cursor = new require('Cursor').Cursor,
            SelectionManager = new require('Selection').SelectionManager;
            Tokenizer = new require('Tokenizer').Tokenizer;
        this.editor = element.npedit;

        this.tokenizer = new Tokenizer('javascript');

        /* Layers */
        this.editor_frame = element;
        this.editor_frame_wdth = this.editor_frame.clientWidth;

        this.left_bar_wdth =  35; // parseInt(this.left_bar.getStyle('width'));
        this.left_bar = new Element('div', {'class' : 'left-bar', styles : {width : this.left_bar_wdth + 'px'}});
        

        /* Take in account the size of the scroll bar */
        this.right_bar_wdth = (this.editor_frame_wdth - this.left_bar_wdth) - 20;

        this.text_frame = new Element('div', {'class' : 'text-layer'});
        this.scroller = new Element('div', {'class' : 'scroller'});

        this.cursor_frame = new Element('div', {
            'class' : 'cursor-layer', 
            styles : {width :  this.right_bar_wdth + 'px', "margin-left" :  this.left_bar_wdth + 'px'}
        });
        this.cursor_frame.renderer = this;
        this.cursor = new Cursor(this.cursor_frame);

        // this.controls = new Element('div', {'class' : 'controls', 'html' : '#'});

        this.selection = new SelectionManager();

        /* Sizes */
        this.CHARS_WIDTH = 8;
        this.CHARS_HEIGHT = 17;
        this.measureNode = null;
        this.measureLength = 1000;

        /* Scrolling */
        this.scrolled = 0;
        this.visibleLines = {first : 0, last : (this.editor_frame.getHeight() / this.CHARS_HEIGHT)};

        /* Counters */
        this.lineCount = 0;
        this.countLines = function () {
            this.lineCount = this.text_frame.getChildren().length;
            return this.lineCount;
        }

        /** @function Display.$measureCharSize
         *  @description Determines the actual sizes in pixel of the
         *      characters drawn by the browser
         */
        this.$measureCharSize = function () {
            if (this.measureNode === null) {
                this.measureNode = new Element("div", {id : 'measure-node'});
                var str = "";
                for(var i = 0; i < this.measureLength; i++){
                    str = str + "Xy";
                }
                this.measureNode.innerHTML = str;
                this.scroller.adopt(this.measureNode);
            }

            this.cursor.CHARS_WIDTH = (this.measureNode.offsetWidth) / (this.measureLength * 2);
            this.cursor.CHARS_HEIGHT = this.measureNode.offsetHeight;
            this.cursor.el.setStyle('height', this.CHARS_HEIGHT + 'px');
        }
        
        /** @class LineNumber
         *  @description Construct a new DOM elememt for a line number in the
         *  left bar
         */
        var $LineNumber = function( id , markers ){
            return new Element('span', {'class':'line-id', 'text' : id.toString()});
        }

        /** @class Display.Line
         *  @description Abstraction of a line ( costructed via MooTools Class )
         *  @todo non si possono documentare i metodi se si utilizza la classe
         *  fornita da mootools
         */
        var Line = new Class({
            initialize : function ( id ){
                if ( id instanceof Element)
                    this.el = id;
                else{
                    this.el = new Element('div', {'class':'line-content', 'id' : 'line_'+id});
                }
                this.ch_width = 8;
            }
        });
        
        Line.implement({
            text : function( txt ){
                if ( txt != undefined) 
                    this.el.set('text', txt);
                return this.el.get('text');
            },
            textLength : function(){ 
                return this.el.get('text').length; 
            },
            empty : function(){
                return this.el.empty();
            }
        });
        
        this.lineContent = function(id){
            var l = this.getLine(id);
            return this.wpDecode(l.text());
        }

        /** @function
         *  @description Modifies the browser selection. This function is 
         *  called when the keyboard is used to modify the selection
         *  @arguments o the horizontal direction of the cursor
         *  @arguments v the vertical direction of the cursor
         */
        this.modify_selection = function (o, v){
            // controlla se c'è una selezione attiva
            console.log(o, v);
            var sel = window.getSelection(),
                cp = this.cursor.position,
                focus = this.getCurrentElement(),
                base;
            if(sel.type == 'None'){
                base = this.getCurrentElement(cp.x - o, cp.y - v);
                sel.anchorNode = base.el;
                sel.anchorOffset = base.offset;
                sel.focusNode = focus.el;
                sel.focusOffset = focus.offset;
            } else {
                sel.extend(focus.el, focus.offset);
            }
            
        }
        
        /** @function
         *  @description Returns the number of characters in a line
         *  (after decoding white spaces)
         */
        this.charsNum = function (id) {
            var l = this.getLine(id);
            return this.wpDecode(l.text()).length;
        };
        
        /** @function Display.drawLine
         *  @description Draw a line and can put text in it, divided in tokens
         *      by injecting span elements
         */
        this.drawLine = function (id, txt, array) {
            var l = new Line(id);
            if (txt){
                if(array){
                    for( var i = 0; i < txt.length; i++){
                        l.el.adopt( new Element('span', { 
                            'class': txt[i].str, 
                            'html' : this.wpEncode(txt[i].txt)
                        }));
                    }
                }else {
                    var toks = this.tokenizer.parse(txt);
                    for( var i = 0; i < toks.length; i++){
                        l.el.adopt( new Element('span', { 
                            'class': this.tokenizer.token(toks[i]).str, 
                            'html' : this.wpEncode(toks[i])
                        }));
                    }
                }
            }
            this.text_frame.adopt( l.el);
            this.lineCount += 1;
        }
        
        /** @function
         *  @description draws the number next to the id-th line displayed
         *  in the editor frame
         *  @todo add markers support
         *
         */
        this.drawLinenumber = function( id, markers){
            this.left_bar.adopt( new $LineNumber(id, markers));
        }

        /** @function
         *  @description Deletes the id-th line by disposing the line element.
         *  @todo Doesn't manage the line number
         *
         */
        this.deleteLine = function( id ){
            /* The first line should never be canceled */
            if(id <= 0 || id >= this.lineCount){
                return;
            }
            var els = this.text_frame.getChildren('#line_'+id);
            this.left_bar.children[this.lineCount - 1].destroy();
            this.lineCount -= 1;
        }
        
        /** @function realOffset
        *   @description Returns the horizontal offset of the cursor in number of character,
        *   without counting their displayed length (i.e. the tab real length is 1
        *    but is displayed with lenght 4)
        *   @arguments l the id of a line (default current line)
        *   @arguments o the displayed offset of the cursor (default current position)
        */
        this.realOffset = function (l, o) {
            var line = (l) ? this.getLine(l) : this.currentLine(),
                offset = (o) ? o : this.cursor.position.x;
            
            var sub = line.text().substring(0,offset);
            var real = this.wpDecode(sub);
            return offset - (sub.length - real.length);
        }

        /**  
         *  @function
         *  @description Ritorna l'elemento del DOM su cui si trova il cursore
         *  o un nuovo elemento se il cursore non è su un elemento
         *  
         */
        this.getCurrentElement = function(x, y){
            
            function tokenLength (tok) {
                return (tok=='\t') ? 4 : tok.length;
            }
            var line, off;
            if( x && y){
              line = this.getLine(y);
              off = x;
            } else {
                line = this.currentLine();
                off = this.cursor.position.x;
            }
            
            if ( off > line.text().length ){
                throw new Error("curretElement: off > line length: " +
                    "line: '"+ line.text() +"' offset: "+ off);
            }
            
            // console.log(line.text());
            // console.log(this.wpDecode('dec: ' + line.text()));
            var els = this.tokenizer.parse(this.wpDecode(line.text()) ), 
                i = 0, // traces how many characters are there
                j = 0, // traces how many characters before last element
                e = 0; // traces the ids of elements
            
            // console.log(els);
            if( els.length == 0){
                var elem = new Element('span');
                line.el.adopt(elem);
                return {el : elem, offset : 0, txt : "", before : "", after : ""};
            }
            
            i += tokenLength(els[e]);
            while( i < off ){
                e++;
                i += tokenLength(els[e]);
                j += tokenLength(els[e-1]);
            }
            
            if ( e < 0 || e > els.length)
                throw new Error("get current element");
            var ch = line.el.getChildren();
            
            var t = ch[e].get('text');
            return { 
                el : ch[e], // the element the cursor is on
                elId : e, // the rank in the order of the element
                offset : off - j, // the offset in the element
                txt : t, // the element text
                before : t.substr(0, off - j), // the element text before cursor
                after : t.substr( off - j), // the element text after cursor
                isLast : (e >= (ch.length - 1)) // el is the last of the line?
            };
        }
        
        this.wpPlaceholder = {
            ch : '◦',
            code : '&#9702;'
        } ;

        this.encodeTable = [
            {en : '→→→→', de : '\t'},
            {en : '◦', de : ' '}
        ];

        /** @function getSeed
         *  @description returns the word the cursor is at the end of. Used to be
         *    the seed for filter the suggestions
         */
        this.getSeed = function() {
            var curr = this.getCurrentElement().txt;
            return this.wpDecode(curr);
        }
        /** @function
         *  @description replaces white spaces with the right placeholder.
         *  This has two goals: first, face that html is whitespace-unsensitive,
         *  second, gives the whitespace the right width.
         *  @arguments str the string to be encoded
         */
        this.wpEncode = function (str) {
            if (str && str.replace){
                var s = str;
                for (var i = this.encodeTable.length - 1; i >= 0; i--) {
                    s = s.replace( RegExp(this.encodeTable[i].de, 'g'), this.encodeTable[i].en);
                };
                return s;
            } else
                return str;
        }

        /** @function
         *  @description replaces the place holders with white spaces
         *  @arguments str the string to be decoded
         */
        this.wpDecode = function (str) {
            if (str && str.replace){
                var s = str;
                for (var i = this.encodeTable.length - 1; i >= 0; i--) {
                    s = s.replace( RegExp(this.encodeTable[i].en, 'g'), this.encodeTable[i].de);
                };
                return s;
            } else
                return str;
        }
        
        /** @function
         *  @description Manages the insertion of a character in the text,
         *   next to the cursor and moves the cursor
         *  @arguments chr the character to be inserted
         */
        this.insert = function (chr) {

            
            var current = this.getCurrentElement(), ntxt, el, ch, nel;
            // console.log(current);
            if (this.tokenizer.isTokenEnding(this.wpDecode(current.before))) {
               current.el.set('text', current.before);
               current.el.set('class', this.tokenizer.token(this.wpDecode(current.before)).str );
               if(!current.isLast){
                    ch = current.el.parentNode.children;
                    nel = ch[current.elId + 1];
                    ntxt = chr + nel.get('text');
                    nel.set('text', ntxt);
                    nel.set('class', this.tokenizer.token(this.wpDecode(ntxt)).str);
               }else{
                    ntxt = this.wpEncode(chr)  + current.after;
                    el = new Element('span',{ 
                            'html' : ntxt,
                            'class' : this.tokenizer.token(this.wpDecode(ntxt)).str
                    });
                    this.currentLine().el.adopt(el);
               }
            } else if( this.tokenizer.isTokenEnding(chr) ){
                var el, el2, ntxt;
               
                ntxt = chr; 
                if(current.before.length != 0){
                    current.el.set('text', current.before);
                    current.el.set('class', this.tokenizer.token(current.before).str );
                    el = new Element('span');
                    el.set('html', this.wpEncode(ntxt));
                    el.set('class', this.tokenizer.token(ntxt).str);
                    el.inject(current.el, 'after');
                } else {
                    el = current.el;
                    el.set('html', this.wpEncode(ntxt));
                    el.set('class', this.tokenizer.token(ntxt).str);
                    this.currentLine().el.adopt(el);               
                }
               
               if(current.after.length != 0){
                   el2 = new Element('span');
                   el2.set('text', current.after);
                   el2.set('class', this.tokenizer.token(current.after).str);
                   el.inject(el, 'after');
               }
            }else{
               var ntxt = current.before + chr + current.after;
               current.el.set('html', ntxt);
               current.el.set('class', this.tokenizer.token(ntxt).str);
            }

        }
        
        this.deleteChar = function (direction) {
            
            var current = this.getCurrentElement(),
                ntxt, step;
            
            if(current.el.hasClass('np_tab'))
                step = 4;
            else
                step = 1;

            if (direction > 0){    // pressed Canc
                if(this.cursor.position.x == this.currentLine().textLength()){
                    this.update();
                    return;
                }
                ntxt = current.before + current.after.substr(step);
            } else {  // pressed BackSpace
                if(this.cursor.position.x == 0){
                    var l = this.currentLine().textLength();
                    this.update();
                    this.movecursor_x(-1 * (l + 1));
                    return;
                }
                ntxt = current.before.substr(0, current.before.length - step) + current.after;
                this.movecursor_x(-step);
            }
            
            // token vuoto
            if(ntxt.length == 0){
                current.el.destroy();
            } else {
                current.el.set('text', ntxt);
                current.el.set('class', this.tokenizer.token(ntxt).str);    
            }
        }

        /* carrier return */
        this.cr = function(){
            this.editor.buffer.insertChar('\n');
            this.update();
            this.movecursor_y(+1);
            this.cursor_to_bol();
        }

        this.newId = function (){
            return this.lineCount + 1;
        }
        
        this.currentLine = function(){
            // console.log("current " + this.cursor.position.y);
            return this.getLine( this.cursor.position.y );
        }
        
        this.precedingLine = function(){
            return this.getLine( this.cursor.position.y - 1 );
        }
        
        this.nextLine = function(){
            return this.getLine( this.cursor.position.y + 1 );
        }

        this.isLastLine = function (){
            return (this.cursor.position.y == (this.lineCount - 1));
        }

        this.appendLine = function (t){
            var id = this.newId()
            this.drawLine( id, t);
            return id;
        }
        
        /* @function 
         * @description returns a line by its id
         * @arg id the line number starting from 0
         */
        this.getLine = function( id ){
            var lines = this.text_frame.getChildren();
            var idx = id;
            if( idx != undefined && idx < lines.length && idx >= 0 )
                return new Line(lines[ idx ]);
            else
                throw new Error("No line " + idx + " (" + lines.length + " lines)" );
        }
        
        /**  @function cursor_to_eol
         *  @description takes the cursor to the end of the line it is on
         */
        this.cursor_to_eol = function (){
            var l  = this.currentLine();
            this.movecursor_x(l.textLength() - this.cursor.position.x);
        }

        /**  @function cursor_to_bol
         *  @description takes the cursor to the beginning of the line it is on
         */
        this.cursor_to_bol = function (){
            this.movecursor_x( -1 *  this.cursor.position.x );
        }

        /**  @function cursor_in_view
         *  @description if not visible, takes the cursor in the center of
         *  the editor, scrolling the editor
         */
        this.cursor_in_view = function(){
            
            var first = Math.floor(this.scroller.scrollTop / this.CHARS_HEIGHT),
                last = Math.floor(this.editor_frame.offsetHeight / this.CHARS_HEIGHT) + first,
                y = this.cursor.position.y;
            
            if (y < first || y > last )
                this.scroll( y - Math.floor((last - first) / 2), 'absolute');
        }
            
        /* @function movecursor_x
         * @description moves the cursor horizontally
         * @arg offset spostamento in orizzontale (in numero di caratteri)
         */
        this.movecursor_x = function (offset) {
            
            var x = this.cursor.position.x,
                y = this.cursor.position.y,
                l;
            
            this.cursor_in_view();
            
            if( x + offset < 0){
                if( ! this.cursor.isOnFirstLine() ){
                    l = this.precedingLine().textLength();
                    return this.cursor.set( l + (offset + 1), y - 1);;
                }
            } else {
                l = this.currentLine().textLength();
                if( x + offset > l ){
                    this.cursor.set( 0 , y + 1);
                } else
                    return this.cursor.set( x + offset , y );
            }
        }
    
        /* @function movecursor_y
         * @description moves the cursor vertically
         * @arg offset spostamento in verticale ( in numero di caratteri)
         */
        this.movecursor_y = function (offset) {
            
            var x = this.cursor.position.x,
                y = this.cursor.position.y;            
            
            this.cursor_in_view();
            
            // scrolling needed?
            if ( y + offset <= this.scrolled ){
                this.scroll( -1 , 'relative');
                y = ( y + offset >= 0 ) ? y + offset : 0 ;
                this.cursor.set( x, y, this.getLine(y).textLength());
                return;
            }
            // bottom of the doc
            if ( y + offset >= this.lineCount ) {
                return;
            }

            if (y + offset >= Math.floor(this.editor_frame.offsetHeight / this.CHARS_HEIGHT) + this.scrolled ) 
                this.scroll( +1, 'relative' );
            
            y = y + offset;
            this.cursor.set( x, y, this.getLine(y).textLength());
        }
        
        /** @function
         *  @description scrolls the scroller layer which contains all
         *  the other layers
         * 
         */
        this.scroll = function( offset, how ){
            
            if ( how == 'relative'){
                if (this.scrolled + offset < 0) {
                    this.scrolled = 0;
                    return;
                }
                this.scroller.scrollTo(0, (this.scrolled + offset) * this.CHARS_HEIGHT );
                this.scrolled += offset;
            }
            if ( how == 'absolute' ){
                if (this.scrolled < 0) { 
                    this.scrolled = 0;
                    return; 
                }
                this.scroller.scrollTo(0, this.scrolled * this.CHARS_HEIGHT );
                this.scrolled = offset;
            }
        }
        

        this.color = function() {
            var markup = this.tokenizer.markup(this.editor.buffer.dumpString());
            var lines = this.text_frame.children;
            for(var i = 0; i < lines.length; i++){
                this.color_line(lines[i], markup[i]);
            }
        }

        this.color_line = function (line, markup) {
            var tokens = line.children;
            for(var i = 0; i < tokens.length; i++){
                tokens[i].set('class', markup[i]);
            }
        }

        /** @function
         *  @description Reloads the text from the text buffer and redraws
         *  all the lines and words
         */
        this.update = function(){
            
            this.text_frame.empty();
            this.left_bar.empty();
            
            // var lines = this.editor_frame.npedit.buffer.getLines();
            var text = this.editor.buffer.dumpString();
            var toks = this.tokenizer.analize(text);
            for (var i = 1; i <= toks.length; i++){
                this.drawLinenumber(i);
                this.drawLine(i, toks[i-1], true);
            }
            this.countLines();
        }
        
        /**@function
         * @description Set the focus to this editor so that the input is driven
         * here
         *
         */
        this.getFocus = function() {
            
            if(!this.focused)
                this.blinkid = setInterval( this.blinker, 500 );
            this.focused = true;
			if(Browser.ie){
				this.editor_frame.setStyle('border-color', 'orange');
			}

            window.netpad.active = this.editor;
        }
		
		this.loseFocus = function() {
			if(Browser.ie){
				netpad.active.display.editor_frame.setStyle('border-color', 'grey');
			}
			clearInterval(this.blinkid);
            this.focused = false;
            this.cursor.show();
		}

        /**
         *   @function Display.Render 
         *   @description Draws the frames of the editor
         */
        this.Render = function (){            
            
            // Lets body get focus in FF
            this.editor_frame.set('tabindex', 0);
            

            // this.input.addEvent('onblur', function(){
            //      this.focused = false;
            //      this.editor_frame.addClass('np_focused');
            // });
            
            
            this.editor_frame.adopt(this.scroller);
            this.cursor_frame.adopt(this.text_frame);
            // this.editor_frame.adopt(this.input);
            // this.editor_frame.adopt(this.controls);
            this.scroller.adopt(this.left_bar, this.cursor_frame);
            
            this.$measureCharSize();
            this.update();
            
            /**@function
             * @description Makes the cursor blink
             * 
             */
            this.blinker = function (c) {
                return function(){
                    c.blink();
                };
            }(this.cursor);
        }
        
                
    }

    exports.Display = Display;
});
