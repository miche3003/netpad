
var src = "/**\n* The number of elements contained in the heap.\n* @type int\n*/\n\
  this.Count = function () { return pos; }\n\
\
  /**\n\
   * Insert a Task into the heap.\n\
   * @param {AnimeJTask} el Task to be inserted.\
   */\
  this.Insert = function (el) {\
    this[pos] = el;\
    percolateUp(pos++);\
  }";

  /**
   * Read the top of the heap. If the heap is empty null is returned.
   * @type AnimeJTask
   */  
  this.Top = function () {
	if (pos)
		return this[0];
	return null;
  }

  /**
   * Remove the top element from the heap and returns it. If the heap
   * is empty null is returned and the heap is left unchanged.
   * @type AnimeJTask
   */  
  this.Remove = function () {
	var ret = null;
	if (!pos) return ret;
	ret = this[0];

	percolateDown(0);
	this[pos--] = null;

	return ret;
  }
  
  /**
   * Remove a specific task from the heap.
   * @param {AnimeJTask} t The task to remove. 
   * @type AnimeJTask
   */
  this.RemoveTask = function (t) {
    var i, ret = -1;
    for (i = 0; i < pos; i++)
      if (this[i].Task == t) {
        ret = this[i].Due;
        percolateDown(i);
	    this[pos--] = null;
        break;
      }
    return ret;
  }

  var patt1 = /\/\*\*[\w\W^(\*\/)]*\*\//;
  var patt2 = /\/\*\*[^\*\/]*\*\//;
  var block = /\{|\{[]\}*\}/;
  var func_patt = /function/;
  var comments = src.match(block);
  if( comments != null)
  	for(var i = 0; i < comments.length; i++)
  		console.log( i + ": \t " + comments[i] + "\n\n");