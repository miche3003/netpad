define(function(require, exports, module){

	var SelectionManager = function(){
		
		this.isEmpty = function(sel) {
			return (sel.type == 'None' || sel.type == 'Caret' || sel.isCollapsed == true);
		}

		this.get = function() {
			var sel = window.getSelection();
			if( this.isEmpty(sel)) {
				return null;
			}
			
			return {
				sel : sel
				, text : sel.toString()
				, empty : function () { this.sel.empty(); }
				, del : function() { 
					if(this.sel.deleteFromDocument) 
						this.sel.deleteFromDocument();
				}
			}
		}
	};

	exports.SelectionManager = SelectionManager;
});