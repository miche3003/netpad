define(function(require, exports, module){

    /** @class */
    Editor = function (el) {

    	var Display = new require('Display').Display,
            Listbox = new require('Listbox').Listbox,
    		Buffer = new require('Buffer').Buffer;

        if(!el){
            throw new Error("Give me a div element!");
            return;
        }

        var editor = el;
        this.syntax_tree = null;
        this.cursorOffset = 0;           // La posizione relativa del cursore all'interno del nodo in cui si trova
        this.lastLine = 0;

        this.close = function () { };
        this.save = function () { 
            var ex = this.buffer.dumpString()
            // console.log(ex);
            return ex; 
        };
        this.export = function() {
            window.open("data:text/plain;charset=utf-8," + this.save() );
        }
        this.load = function (txt) { 
            this.buffer.clear();
            this.buffer.insertString(txt); 
            this.display.update(); 
            this.display.cursor.toOrigin();
        };
        this.setLanguage = function (lang){
            this.display.tokenizer.setLang(lang);
        }
        /** @function Editor.Init
         *  @description Init the editor
         *  @arg
         */
        this.init = function () {

            /** @todo il contenitore dell'editor può essere un'altro? */
            this.editor = el;
            this.editor.npedit = this;
            
            /** se contiene testo, viene tolto ed inserito nel buffer */
            var initial_txt = this.editor.get('text');
            if (initial_txt !== "") {
                this.editor.set('text', "");
            }

            this.buffer = new Buffer();
            this.buffer.insertString( initial_txt );
                
            this.display = new Display( this.editor );
            this.display.Render();
            
            this.listbox = new Listbox( this.display.cursor_frame, function(){}, ['fragole']);
            this.listbox.defaultCb = function(editor){
                return function (data){
                    if(typeof data != 'string') return;
                    data = data + " ";
                    editor.buffer.insertString(data);
                    for(var i = 0; i < data.length; i++){
                        editor.display.insert(data[i]);
                        editor.display.movecursor_x(1);
                    }
                };
            }(this);
            this.buffer.pointSet(0, 0);
           }
    }
    exports.Editor = Editor;
});