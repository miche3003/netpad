
define(function(require, exports, module){
    
    MouseInput = function () {
        

        var updateCursor = function (event, display){
            var pos = display.cursor_frame.getPosition();
            var x = Math.floor( (event.client.x - pos.x) / display.CHARS_WIDTH),
                y = Math.floor( (event.client.y - pos.y) / display.CHARS_HEIGHT);
            try {
                display.cursor.set(x, y, display.getLine(y).textLength());
            } catch(error) {}
        }

        window.addEvent('click', function(e){ 
          
          if(netpad.active){
            if(!netpad.active.display.editor_frame.contains(e.target)){
				window.console.log(netpad.active.display);
				netpad.active.display.loseFocus();
				netpad.active = null;
            } else {
                updateCursor(e, netpad.active.display);
            }
          }

          for(var i = 0; i < netpad.editors.length; i++)
          if(netpad.editors[i].display.editor_frame.contains(e.target)){
            netpad.editors[i].display.getFocus();
          }

        });

        window.addEvent('mouseup', function(e){ 
          if(netpad.active && window.getSelection().type != 'None'){
            updateCursor(e, netpad.active.display);
          }
        });
    };
    
    exports.MouseInput = MouseInput;
});

