define(function(require, exports, module){
	
	var Clipboard = function(np){
		window.addEvent('paste',function(e){
	            var txt = e.event.clipboardData.getData('text');
	            console.log(txt);
	            if(np.active){
	                console.log(txt);
	                np.active.buffer.insertString(txt);
	                np.active.display.update();
	                np.active.display.movecursor_x(txt.length);
	        }
	    });
	}

	exports.Clipboard = Clipboard;
});