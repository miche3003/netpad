
define( function(require, exports, module){
    
    KeyboardInput = function (np) {
        var editor = np.active;
        

        this.recolor = function(editor) {
            return function(){
                editor.display.color();
                editor.recolor_timer = false;
            }
        }
        
        /***********************
                EVENTS 

        ***********************/

        window.addEvent('carrier_return', function(editor, cp){
            editor.buffer.pointSet( cp.x, cp.y + 1);
            editor.display.cr();
        });

        // @todo inserzione e cancellazione di intere stringhe
        // @todo il seed per la listbox deve supportare la dot notation

        window.addEvent('insertion', function(e){
            var editor = e.editor, c = e.char;
            var cp = editor.display.cursor.position;
            // cp.x = editor.display.realOffset(cp.y, cp.x);
            console.log(cp);
            editor.buffer.pointSet(cp.x, cp.y+1);
            editor.buffer.insertChar(c);
            editor.display.insert(c);
            editor.display.movecursor_x(1);
        });

        window.addEvent('deletion', function(e){
            // console.log(e);
            var cp = e.editor.display.cursor.position;
            var real = e.editor.display.realOffset(cp.y, cp.x);
            e.editor.buffer.pointSet(real, cp.y+1);
            e.editor.buffer.deleteChars(e.offset);
            if (e.removeFromDisplay)
                e.editor.display.deleteChar( e.offset );
            else
                e.editor.display.movecursor_x(e.move);
        });

        this.keyBindings = {
            13 : function () { // return
                if(editor.listbox.active){
                    var f = function(editor){
                        return function (data){
                            if(typeof data != 'string') return;
                            data = data + " ";
                            editor.buffer.insertString(data);
                            for(var i = 0; i < data.length; i++){
                                editor.display.insert(data[i]);
                                editor.display.movecursor_x(1);
                            }
                            
                        };
                    }(editor)
                    editor.listbox.hide(f);
                    return;
                }
                var cp = editor.display.cursor.position;
                cp.x = editor.display.realOffset(cp.y, cp.x);
                window.fireEvent('carrier_return', [editor, cp]);
            },
            35 : function () {  // end @todo @bug not moving the buffer point
                editor.display.cursor_to_eol();
                event.preventDefault();
            },
            36 : function () { // home @todo @bug not moving the buffer point
                editor.display.cursor_to_bol();
            },
            8 : function (e){ // backspace
                e.preventDefault();
                var sel = editor.display.selection.get(), l;
                if(sel) {
                    l = sel.text.length;
                    window.fireEvent('deletion', {editor: editor, offset: l * -1, move: -1 * l});
                    sel.del();
                } else 
                window.fireEvent('deletion', 
                    {editor: editor, offset: -1, move: -1, removeFromDisplay: true}
                );
				e.stop();

                return false;
            },
            // canc
            46 : function() {
                window.fireEvent('deletion', {editor: editor, offset: 1, move: 0});
            },
            // tab
            9 : function() {
                editor.buffer.insertChar('\t');
                editor.display.insert('\t');
                editor.display.movecursor_x(4);
                // event.preventDefault();
                // event.stopPropagation();
                return false;
            }
            , 27 : function(){
                if(editor.listbox.active)
                    editor.listbox.hide();
            }
        }

        window.addEvent('keypress', function (event) {
            // console.log(event);
            if(!netpad.active) return true;
            var chars = {
                'space' : ' ',
                'left' : "%",
                'right' : "'",
                'delete': '.',
                'down' : '(',
                'up' : '&'
                },
                skip = [ 
                    'enter'
                    , 'backspace'
                    , 'esc'
                    , '%'
                    , 'left'
                    , 'right'
                    // , 'up'
                    // , 'down' 
                ];

            var cb = { 
                '(' : function(){ a.buffer.insertChar(')'); a.display.insert(')'); },
                '{' : function(){ a.buffer.insertChar('}'); a.display.insert('}'); }
            };


            // console.log(event);
            var a = window.netpad.active, k;
            if( 
                // window.webkitURL && 
                chars[event.key]){
                k = chars[event.key];
                // console.log('char found:'+ k);
            } else
                k = event.key;

            if (event.code >= 65 && event.code <= 90 && event.shift)
                k = k.toUpperCase();

            if(skip.contains(event.key) || event.event.charCode == 0){
                return;
            }
            
            var sel = editor.display.selection.get(), l;
                if(sel) {
                    l = sel.text.length;
                    window.fireEvent('deletion', {editor: editor, offset: l * -1, move: -1 * l});
                    sel.del();
            }
            
            if(a.listbox.active){
                a.listbox.updateItems(k);
            }

            window.fireEvent('insertion', { editor: a, char: k});

            // if(cb[k]){
            //     cb[k]();
            // }

            if(a.recolor_timer)
                clearTimeout(a.recolor_timer)
            a.recolor_timer = setTimeout( this.netpad.keyboardInput.recolor(editor), 1000);

            event.preventDefault();
            return false;

        });
        
        window.addEvent('keydown', function (event){
            if(np.active){
                if(event.control){
                    // console.log('control+' + event.key);
                    if(event.key == 'c'){
                        var txt = window.getSelection().toString();
//                        copy(np.active.display.wpDecode(txt));
                    }
                    if (event.key == 'space'){
                        var pos = np.active.display.cursor.basePosition();
                        np.active.listbox.show(np.active.display.getSeed(), pos);
                        event.preventDefault();
                    }
                    // event.preventDefault();
                    return true;
                }
                // Function Keys
                if(event.code >= 112 && event.code <= 123){
                }
                editor = np.active;
                switch(event.code){
                    case 37:
                    case 39:
                        // moving left or right
                        var step = (event.code == 37) ? -1 : 1;
                        editor.display.movecursor_x(step);
                        if(event.shift){ // moves the selection
                            editor.display.selection(step, 0);
                        } else {
                            var sel = window.getSelection();
                            if (sel.type != 'None' && sel.toString() != ""){
                                sel.empty();
                            }
                        }
                        if(editor.listbox.active) editor.listbox.hide();
                        event.preventDefault();
                        return false;
                        break;
                    case 38:
                    case 40:
                        // moving up or down
                        var step = (event.code == 38) ? -1 : 1;
                        if(editor.listbox.active){
                            editor.listbox.move(step);
                        }else {
                            editor.display.movecursor_y(step);
                            if(event.shift){
                                editor.display.selection(0, step);
                            } else {
                                var sel = window.getSelection();
                                if (sel.type != 'None' && sel.toString() != ""){
                                    console.log(sel.toString());
                                    // sel.empty();
                                }
                            }
                        }
                        event.preventDefault();
                        break;
                    default:
                        if (np.keyboardInput.keyBindings[event.code]){
                            return np.keyboardInput.keyBindings[event.code](event);
                        }
                }
            }
            // return false;
        });
        
    }

    exports.KeyboardInput = KeyboardInput;
});

/*
    NOTE
        caratteri a-z da 65 a 90
        cifre dec 0-9 da 48 a 57
*/
