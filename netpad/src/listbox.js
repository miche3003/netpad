
define(function(require, exports, module){
    
    var ListBox = function (parent, callback, items) {
        // var Fx = (new require('netpad/lib/mootools')).Fx; @todo
        this.box = new Element('div', { id : 'np_listbox', styles : {
            'height': '70px',
            'overflow-y': 'scroll'
        }});

        this.box.listbox = this;
        
        this.selected = null;
        this.active = false;
        
        this.scrolled = 0;
        this.scrolledPx = 0;
        this.itemsHeight = 17;

        this.items = items || [];

        // this.slide = new Fx.slide(this.box); @todo

        var ListItem = function (content, id, data, list_id) {
            
            var it =  new Element( 'a', { 
                href : "", 
                'class' : 'np_listbox_item',
                'html' : content,
                'id' : id,
                'data': data,
                'list_id': list_id,
                'styles' : { 'color' : 'black'}
            });
            
            it.addEvent('click', function(e){
                this.parentNode.listbox.select(this);
                this.parentNode.listbox.hide();
                e.stop();
                return false;
            });

            return it;
            
        }
        
        this.addItems = function (it) {
            if(typeof it == 'string'){
                this.items.push(it);
            } else {
                this.items = this.items.concat(it);
                /* console.log(it, this); */
            }
            this.setItems();
        }

        this.hasSeed = function (seed) {
            return function(x){
                return (x.substr(0, seed.length) == seed); 
            }
        }

        this.setItems = function (seed) {
            var new_item, res, len, str, data;
            
            if(seed && seed.length){
                this.seed = seed;
                res = this.items.filter(this.hasSeed(seed));
            }else {
                res = this.items;
            }
            // console.log(seed, res);
            this.box.empty();
            for (var i = res.length - 1; i >= 0; i--) {
                data = (seed) ?  res[i].substr(seed.length) : res[i];
                str = (seed) ? '<b>' + seed + '</b>' + res[i].substr(seed.length) : res[i];
                var new_item = new ListItem(str, '', data, res.length  - (i + 1));
                this.box.adopt(new_item);
            }
            if(this.box.children.length){
                // this.itemsHeight = this.box.children[0].clientHeight;
            }
        };
        
        this.updateItems = function(k) {
            if (this.seed && this.seed.length)
                this.seed += k;
            else
                this.seed = k;
            this.setItems(this.seed);
        }

        this.hide = function(cb) {
            this.box.setStyle('display', 'none');
            this.active = false;
            if(this.selected)
                var data = this.selected.el.get('data');
            this.select('none');
            fn = cb || this.defaultCb;
            if(data && fn)
              return(fn(data));
        }
        
        this.show = function (seed, pos) {
            if (! this.active ){
                if(pos) this.setPosition(pos);
                if(seed) this.setItems(seed);
                this.active = true;
                this.box.setStyle('display', 'block');
                this.select(0);

                var y = parseInt(this.box.getStyle('margin-top')), y2 = this.box.parentNode.clientHeight;

                if (y2 - y < this.box.clientHeight)
                    this.box.setStyle('height', (y2 - y - 10) + 'px');
            }
        }
        
        this.getPosition = function() {
            return { y : this.box.getStyle('top') , x : this.box.getStyle('left')} ;
        }
        
        this.setPosition = function (pos) {
            this.box.setStyle( 'margin-top', pos.y);
            this.box.setStyle( 'margin-left', pos.x);
        }
        
        this.up = function (){ 
            this.itemsInView = Math.floor(this.box.clientHeight / this.itemsHeight);
            if (this.selected.id == 0){
                this.scroll(this.box.children.length-1);
            }
            else if (this.scrolled == this.selected.id )
                this.scroll(this.scrolled - 1);
            
            this.select( this.selected.id - 1);
        }
        
        /**
            y quanto scrollare, in numero di items
        */
        this.scroll = function (y) {
            this.scrolled= y;
            this.scrolledPx = y*this.itemsHeight;
            this.box.scrollTo(0, this.scrolledPx);
        }

        this.down = function(){
            this.itemsInView = Math.floor(this.box.clientHeight / this.itemsHeight);
            if (this.selected.id == this.box.children.length -1 ){
                this.scroll(0);
            } else if ( this.scrolled + this.itemsInView - 1 == this.selected.id)
                this.scroll(this.scrolled + 1);
            this.select( this.selected.id + 1 ); 
        }
        
        this.move= function (step) {
            if( step > 0) this.down();
            else this.up();
        }

        /**
            @args item dom node or element idx or 'none'
        */

        this.select = function (item) {

            if (this.selected && this.selected.el ) this.selected.el.set('id', '');

            if (item == 'none') { 
                this.selected = null;
                return;
            }
            
            if(typeof item == 'number'){
                var children = this.box.getChildren();
                if(children.length == 0) { return; }
                var id = (item < 0) ?
                        children.length + ( item % children.length ) : 
                        item % children.length ;
                var el = children[id];
                this.selected = {el: el, id: id};
                
            }else {
                this.selected = { el: item, id: item.list_id };
            }
            this.selected.el.set('id', 'selected');
        }   
        
        this.box.inject(parent);
        if (items)
            this.addItems(items);
        
        this.box.addEvent('click', function (e) {
            window.lb.hide();
            e.stopPropagation();
        });
       
    };
   
   exports.Listbox = ListBox;
});