
/** @fileoverview Il file principale - inizializza le strutture dati, avvia l'editor
 *  @author Michele Carignani
 */
/*jslint white : true, newcap : true */
/*global define : true, $ : true, Editor : true */
define(function (require, exports, module) {

    /** @class */
    Netpad = function (els) {

        var KeyboardInput = new require('input/keyboardInput').KeyboardInput,
            MouseInput = new require('input/mouseInput').MouseInput,
            Clipboard = new require('input/Clipboard').Clipboard,
            Editor = new require('Editor').Editor;
        
        window.netpad = this;
        this.selector = '.netpad-editor';
        this.editors = [];
        this.active = null;

		this.allEditors = function (cb) {
			for(var i = 0; i < this.editors.length; i++){
				cb(this.editors[i]);
			}
		}
		
		this.showHiddenChars = function() {
			document.styleSheets[0].addRule('.np_whitespace','color:black');
			this.allEditors(function(e){e.display.update();});
		}
		
		this.hideHiddenChars = function() {
			document.styleSheets[0].addRule('.np_whitespace','color:white');
			this.allEditors(function(e){e.display.update();});
		}
		
        if(els){
            if(typeof els == 'string'){
                this.selector = els;
                els = null;
            }
        }

        var htmlElements = (els) ? els : $$(this.selector);

        for(var i = 0; i < htmlElements.length; i++){
            this.editors[i] = new Editor(htmlElements[i]);
            this.editors[i].init();
        }

        this.keyboardInput = new KeyboardInput(this);
        this.mouseInput = new MouseInput(this);
        this.Clipboard = new Clipboard(this);

        window.addEvent('copy',function(e){
            console.log(e);
        });
        
        window.addEvent('beforecopy',function(e){
            console.log(e);
        }); 

    }
    exports.Netpad = Netpad;
    
});
