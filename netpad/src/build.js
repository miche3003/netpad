/* 
 * Configuration the r.js optimazer
 * For reference check https://github.com/jrburke/r.js/blob/master/build/example.build.js
 */
({
	// appDir: '.',
    baseUrl: ".",
    // dir: "../../netpad-0.1/",
    optimize: 'uglify',
    optimizeCss: "standard",

/*
    cssIn: "../../css/netpad.css",
    out: "css/netpad.css",
*/
    paths: {
    	'lib' : '../lib',
    	'css' : '../css'
    },
    modules: [
    	{name: "main"},
    	// {name: "css/netpad.css", }
    ]
})

