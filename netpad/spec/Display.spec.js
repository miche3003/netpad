
describe("Display", function(){
       
    var d; 
    var lines = [ 
        "the content of a line",
        "the content of another line",
        "this is the line which contains the longest text in itself" ,
        "this is a line with a\ttab"
    ];
        
    beforeEach(function(){
        // np = new (require('Netpad')).Editor(new Element('div'));
        d = new Display(new Element('div'));
        
        this.addMatchers({
            toBeInstanceOf : function(expected){
                return (this.actual instanceof expected);
            },
            toBeObject : function(expected){
                return (this.actual instanceof Object);
            }
        });
        
    });
    
    it("is an object that displays the editor in the page", function(){
        expect(Display).toBeDefined();
        expect( new Display(new Element('div'))).toBeObject();
    });
    
    /* 
     *    Cursor 
     */
    
    it ('places the cursor at the top left corner at the begin',
        function(){
            expect(d.cursor.position).toEqual({x : 0, y: 0});
    });
    
    it('counts the number of lines of text inserted', function(){
        for (var i = 0; i < lines.length; i++)
            d.appendLine(lines[i]);
        expect(d.lineCount).toEqual(lines.length);
        expect(d.countLines()).toEqual(lines.length);
    });
    
    it('appends a new line', function(){
        var str = "the content of a line";
        var id = d.appendLine( str );
        var l = d.text_frame.getChildren().length;
        expect(l).toEqual(1);
    });
    
    it('retrieves a line', function(){
        
        var id = 0;
        expect(d.countLines()).toEqual(0);
        for (var i = 0; i < lines.length; i++)
            d.appendLine(lines[i]);
        var l = d.getLine(id);
        expect(l).toBeInstanceOf(Object);
        expect(l.el).toBeInstanceOf(Object);
        expect(d.lineContent(id)).toEqual(lines[id]);
        
    });
    
    
    it('gets the DOM element the cursor is on',function(){
        
        var input =[ { txt : 'this is true', offset : 10, res : {
                        offset : 2,
                        txt : 'true',
                        before : 'tr',
                        after : 'ue'
                     }},
                     { txt : '' , offset : 0, res: {
                        offset : 0,
                        txt : '',
                        before : '',
                        after : ''
                     }},
                     { txt: 'ciao cari', offset : 5 , res : {
                        offset : 0,
                        txt : 'cari',
                        before : '',
                        after : 'cari'
                     }} ],
            e, c, i;
        
        for(i = 1; i <= input.length; i++){
            d.drawLinenumber(i);
            d.drawLine(i, input[i-1].txt);
        }

        for(i = 0; i < input.length; i++ ){
            // Move the cursor
            d.cursor.set(input[i].offset, i, input[i].txt.length);
            // Get the element
            c = d.getCurrentElement();
            
            expect(c).toBeDefined();
            expect(c.el).toBeDefined();
            expect(c.offset).toEqual(input[i].res.offset);
            expect(c.txt).toEqual(input[i].res.txt);
            
        }
    });
    
    /* LINES */
    
    it('gives the number of characters in a line', function(){
        var id;
        
        for( var i = 0; i < lines.length; i++){
             id = d.appendLine( lines[i] );
             var l = d.getLine(i);
             expect( l.textLength).toBeDefined();
             expect( d.charsNum(i) ).toEqual(lines[i].length);
        }
        
    });
    
    it('displays the text in buffer line by line', function(){
        
    });
    
    it('moves the cursor rigth, down, left and up', function(){
        
        for( var i = 0; i < lines.length; i++){
            d.appendLine( lines[i] );
        }
        
        expect(d.cursor.position).toEqual({x :0, y: 0});
        d.movecursor_x(1);
        expect(d.cursor.position).toEqual({x : 1, y: 0});
        d.movecursor_y(1);
        expect(d.cursor.position).toEqual({x : 1, y: 1});
        d.movecursor_x(-1);
        expect(d.cursor.position).toEqual({x : 0, y: 1});
        d.movecursor_y(-1);
        expect(d.cursor.position).toEqual({x : 0, y: 0});
    });

    /* Insertions */

    xit('inserts characters', function(){
        d.Render();

        var str = "var var";
        for (var i =  0; i < str.length; i++) {
            d.insert(str[i]);
        };
        
        expect(d.currentLine.text).toEqual(str);
    });
    
    it('encodes and decodes string', function(){
        var str = [ "ciao a tutti", '   ' ];
        for(var i = 0; i < str.length; i++){
            expect(d.wpDecode(d.wpEncode(str[i]))).toEqual(str[i]);
        }
    });

    it('returns the cursor offset in terms of characters', function(){
        var input = [ 
            {string: "\tci\ta tut ti", cursor_x : 12, res : 6},
            {string: "\ta", cursor_x : 5, res : 2}
        ];
        for(var i = 0; i < input.length; i++){
            d.drawLine(i, input[i].string)
            expect(d.realOffset(i, input[i].cursor_x)).toEqual(input[i].res);
        }
    });
});


