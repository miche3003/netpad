
describe("Tokenizer", function(){        
        
        var t;
        
        beforeEach(function(){
            t  = new Tokenizer("javascript");
        });
        
        it("is an object that recognizes and split the source code into tokens",function(){
            expect(Tokenizer).toBeDefined();
            expect( typeof new Tokenizer("javascript") ).toEqual("object");
        });
        
        it("its constructor takes a language",function(){
          
        });

        it("determines the semantic of a word", function(){
            expect(t.token("function").type).toEqual( { type: "reserved", name: "FUNCTION"}.type );
            expect(t.token("return").type).toEqual( { type: "reserved", name: "RETURN"}.type );
            expect(t.token("true").type).toEqual( { type: "literal", name: "BOOL_TRUE"}.type );
            expect(t.token("2").type).toEqual( { type: "literal", name: "number"}.type );
            expect(t.token("x").type).toEqual( { type: "unknown", name: "unknown"}.type );
            expect(t.token(" ").type).toEqual( { type: "whitespace", name: "unknown"}.type );
        });
        
        it('tokenize tabulation', function(){
            var res = t.token('\t');
            expect(res.type).toEqual('whitespace');
            expect(res.name).toEqual('tab');
        });

        it("parses a string and returns a list of tokens", function(){
           
           var j;
           var cases = [ 
               // test case 1
               { 
                 test : "function(){console.log('Hello');}", 
                 res :  [ "function", "(", ")", "{", 'console', '.', 'log', "(", "'Hello'", ")", ";","}" ] 
               },
               // test case 2
               { 
                   test : "function trial", 
                   res : [ "function", " ",  "trial"] 
               },
               // test case 3
               {
                   test : "var x = y + 1;",
                   res : ["var", " ", "x", " ", "="," ","y", " ", "+", " ", "1", ";"]
               },
               {
                   test : " ",
                   res : [ " " ]
               },
               {
                   test : "",
                   res : [ ]
               },
               {
                   test : "ciao\ta\ttutti",
                   res : [ "ciao","\t","a", "\t", "tutti" ]
               }
           ];
           
           // go through all cases
           for (var k = 0; k < cases.length; k++){
              var a = t.parse( cases[k].test );
              expect(a.length).toEqual(cases[k].res.length);
              for(var i = 0; i < cases[k].res.length; i++){
                expect( a[i] ).toEqual(cases[k].res[i]);
              }
           }
           
        });
});
