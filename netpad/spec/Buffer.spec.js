

describe("Buffer", function(){
   
   var b;
   
   beforeEach( function () {
      b = new Buffer(); 
   });
   

   /* Getting state information */
   
   it('gets the number of lines in the buffer', function(){
      b.insertString("This is a line\n and this is the second \n third");
      expect(b.getLineNum()).toEqual(3);
   });
   
   it('gets the absolute position of the point', function(){
      expect(b.pointGet()).toEqual(0);
      var s = "Hello";
      b.insertString(s);
      expect(b.pointGet()).toEqual(s.length);
   });
   
   it('gets the line the point is on', function(){
      var s = "This is a line\n and this is the second \n third"
      b.insertString(s);
      expect(b.pointGetLine()).toEqual(3);
   });
   
   it("get the number of lines in the buffer", function(){
       var s = "First line\nand the second\nand the third";
       b.insertString(s);
       
      expect(b.getLineNum()).toEqual(3);
      expect(b.Get_Num_Lines()).toEqual(3);
   });
   
   it('dumps the text content as a string', function(){
      var s = "This is a line\n and this is the second \n third"
      b.insertString(s);
      expect(b.dumpString()).toEqual(s);
   });
   
   it('returns the lines of text as an array of strings', function(){
      var s = "This is a line\n and this is the second \n third"
      b.insertString(s);
      var a = s.split('\n'), c = b.getLines();
      expect(c instanceof Array).toBeTruthy();
      for(var i = 0; i < a.length; i++)
          expect(c[i]).toEqual(a[i]);
   });


   /* Point setting */
   
   it("moves the point to the start", function(){
       b.insertString("Hello guys\n how are you?");
       b.pointToBeginning();
       expect(b.pointGet()).toEqual(0);
       expect(b.pointGetLine()).toEqual(1);
   });

    it("moves the point backward", function(){
       var s = "Hello guys\n how are you\n i'm so fine?";
       var shift = -5;
       b.insertString(s);
       b.pointShift( shift );
       expect(b.pointGet()).toEqual( s.length + shift );
       
    });
    
    it("moves the point to an arbitrary position", function(){
       var s = "Hello guys \nhow are you?\n i'm so fine!";
       var shift = -5;
       b.insertString(s);
       b.pointSet(4, 2);
       expect(b.pointGet()).toEqual( 16 );
    });

   /* INSERTIONS */
   
   it("inserts a character in an empty buffer", function(){
       var c = 'e';
       b.insertChar(c);
       expect(b.start().$c).toEqual(c);
   });
   
   it("inserts a string in an empty instance", function(){
       var s = 'the string to be inserted in the buffer';
       b.insertString(s);
       expect(b.dumpString()).toEqual(s);
   });
   
   it("inserts a character in a non empty buffer", function(){
       var c = 'e';
       var s = "Text inserted before ";
       
       b.insertString(s);
       b.insertChar(c);
       
       expect(b.dumpString()).toEqual(s + c);
   });
   
   it("inserts a string in a non empty instance", function(){
       var s1 = 'the string to ';
       b.insertString(s1);
       
       var s2 = 'be inserted in the buffer';
       b.insertString(s2);
       
       expect(b.dumpString()).toEqual(s1 + s2);
   });

   it("inserts a new line in a non empty instance", function(){
       var s1 = 'The first line\nand the second line';
       b.insertString(s1);
       
       var s2 = '\nbe inserted in the buffer';
       b.insertString(s2);
       
       expect(b.dumpString()).toEqual(s1 + s2);
       expect(b.getLines().length).toEqual(3);
   });
   
   /* DELETIONS */
   
   it("empties the buffer", function(){
      var s =  "Some text long long long long long long long long";
      b.insertString(s);
      b.clear();
      expect(b.dumpString()).toEqual("");
   });
   
   it("deletes some characters just before the point a the end of the buffer", 
      function(){
      var s = "this is a stringr";
      var howmany = -6;
      b.insertString(s);
      b.deleteChars(howmany);
      expect(b.dumpString()).toEqual(s.substring(0, s.length + howmany));
   });
   
   it("deletes the character just before the point at the end of the buffer", function(){
      var s = "this is a stringr", howmany = -1;
      b.insertString(s);
      b.deleteChars(howmany);
      expect(b.dumpString()).toEqual(s.substring(0, s.length + howmany));
   });

   it("deletes the character just after the point at the end of the buffer", function(){
      var s = "this is a stringr", howmany = 1, move = -1;
      b.insertString(s);
      b.pointShift(move);
      b.deleteChars(howmany);
      expect(b.dumpString()).toEqual(s.substring(0, s.length - howmany));
   });
   

   it("deletes some characters just before the point a the end of the buffer", function(){
      var s = "this is a stringr", howmany = 6, move = -9;
      b.insertString(s);
      b.pointShift(move);
      b.deleteChars(howmany);
      expect(b.dumpString()).toEqual("this is ngr");
   });

   
   
});
