/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



describe("Line",function(){
    
    var l;
    
    beforeEach(function(){
       l = new Line(); 
       
        this.addMatchers({
            toBeInstaceOf : function(expected){
                return (this.actual instanceof expected);
            },
            toBeObject : function(expected){
                return (this.actual instanceof Object);
            }
        });
            
    });
    
    it("is an object",function(){
       expect(Line).toBeDefined();
       expect(Line).toBeObject();
    });
    
    it("refers a dom element", function(){
        expect(l.el).toBeInstasceOf(Element);
    });

    it("contains the words as dom children",function(){
        
    })
})
